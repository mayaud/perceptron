Victor MAYAUD
Hichem KHETTAB

--------------------------------Python Code--------------------------------

-perceptron.py: Contains a Perceptron class utilizing a logarithmic loss function as its cost function.

-perceptron2.py: Contains a Perceptron class that uses mean squared error as its cost function.

-experiment.ipynb: Demonstrates the training of our Perceptrons on a selected dataset and presents the results of these training experiments.