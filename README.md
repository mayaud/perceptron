# Projet Perceptron : Classification de Chiffres Manuscrits

Ce dépôt GitHub est dédié à l'implémentation et à l'utilisation d'un algorithme de perceptron pour classer des chiffres manuscrits en tant que chiffres '0' ou '1'. Le projet utilise des concepts fondamentaux d'apprentissage automatique pour construire un classificateur binaire à partir de zéro.

## Structure du Dépôt

- `perceptron.py` : Le fichier Python contenant l'implémentation du perceptron. Ce fichier comprend des méthodes pour l'entraînement (`train`) et la prédiction (`predict`).
- `experiment.ipynb` : Un notebook Jupyter pour exécuter des expérimentations utilisant l'ensemble de données de chiffres manuscrits de UCI, disponible dans scikit-learn.

## Objectifs du Projet

1. **Comprendre le Perceptron** : Découvrir les principes de base de l'algorithme du perceptron et sa mise en œuvre.
2. **Expérience avec le Gradient** : Renforcer la compréhension de la dérivation et de l'algorithme de descente de gradient.
3. **Processus de Machine Learning** : Se familiariser avec les étapes de formation, de validation et de test en manipulant les données.
4. **Amélioration des Compétences en Programmation** : Améliorer la maîtrise des outils de codage et des bibliothèques pour l'apprentissage automatique en construisant un projet complet.

## Installation et Utilisation

1. Clonez ce dépôt sur votre machine locale.
2. Installez les dépendances nécessaires, notamment Python et les bibliothèques scikit-learn.
3. Exécutez le notebook `experiment.ipynb` pour voir les étapes détaillées et les résultats des expériences.

## Documentation

Le code dans `perceptron.py` est entièrement documenté pour faciliter la compréhension et la maintenance. Les détails sur l'utilisation des données et les choix de modélisation sont inclus dans le notebook Jupyter.

## Rapport de Projet

Un rapport détaillé accompagne ce projet, expliquant la méthodologie d'entraînement, les résultats obtenus avec l'ensemble de validation, et les stratégies utilisées pour choisir le modèle final. Le rapport inclut également une discussion sur la performance du modèle sur l'ensemble de test.

