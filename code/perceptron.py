import numpy as np

class Perceptron:

    # Initializes the perceptron with data, weights, bias, and learning rate.
    def __init__(self,X,y, learning_rate=0.001, momentum=0.9,methode="momentum"):
        self.y=y
        self.X=X
        self.initialize_weights(X.shape[1])
        self.b=np.random.randn(1)
        self.dW=0
        self.db=0
        self.learning_rate=learning_rate
        self.log_loss_old=np.inf
        self.momentum = momentum
        self.velocity_W = np.zeros(X.shape[1])
        self.velocity_b = 0
        self.methode=methode

    # Use the He method for initializing weigths.
    def initialize_weights(self, n_in):
        n_out = 1  
        limit = np.sqrt(2 / (n_in + n_out))
        self.W = np.random.randn(n_in) * limit

    # Trains the perceptron over a number of iterations (epochs).
    def train(self,n):
        for _ in range(n):
            self.A = self.sigmoïde(self.X)
            
            if self.methode!="momentum":
                self.update_grad()
                self.update_LearningRate(1) 
            else:
                self.update_momentum()

    # Predicts output using the learned weights and sigmoid activation.
    def predict(self,X):
        return self.sigmoïde(X)

    # Computes the gradient of the cost function with respect to weights and bias.
    def gradient(self):
        dZ = self.A - self.y           
        dW = np.dot(self.X.T, dZ) / len(self.y)
        db = np.sum(dZ) / len(self.y)
        return dW, db
    
    # Updates the weights and bias terms based on the calculated gradient.
    def update_grad(self):
        self.dW,self.db=self.gradient()
        self.log_loss_old=self.log_loss()
        self.W=self.W-self.learning_rate*self.dW
        self.b=self.b-self.learning_rate*self.db
        self.log_loss_new=self.log_loss()


    #Update the weigth and bias with momentum method
    def update_momentum(self):
        grad_W, grad_b = self.gradient()

        # Update velocities for weights and bias
        self.velocity_W = self.momentum * self.velocity_W + self.learning_rate * grad_W
        self.velocity_b = self.momentum * self.velocity_b + self.learning_rate * grad_b

        # Update weights and bias using velocities
        self.W -= self.velocity_W
        self.b -= self.velocity_b


    # The sigmoid activation function that maps the input X to a probability.
    def sigmoïde(self,X):
        Z= X.dot(self.W) + self.b
        A= 1/(1+np.exp(-Z))
        return A

    # Calculates the log loss (cross-entropy loss) for the current predictions.
    def log_loss(self):
        eps=1e-10 #in oder to avoid log(0)
        self.A = self.sigmoïde(self.X)
        return np.sum(-self.y * np.log(self.A+eps) - (1-self.y)*np.log((1-self.A)+eps))/ len(self.y)

    # Dynamically adjusts the learning rate based on the change in log loss.
    def update_LearningRate(self,mode=0):
        if self.log_loss_new < self.log_loss_old:
            if mode==1:
                self.learning_rate*=1.2
            else:
                self.learning_rate*=1
        else:
            if mode == 1:
                self.learning_rate*=0.8
            else:
                self.learning_rate*=1

        