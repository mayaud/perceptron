import numpy as np

class SigmoidNeuron:

    # Initializes the perceptron with data, weights, bias, and learning rate.
    def __init__(self, X, y, learning_rate=0.01, momentum=0.9):
        self.y = y
        self.X = X
        self.w = None
        self.b = np.random.randn(1)
        self.learning_rate = learning_rate
        self.initialize_weights(X.shape[1])
        self.momentum = momentum
        self.velocity_w = np.zeros(X.shape[1])
        self.velocity_b = 0

    # Use the He method for initializing weigths.
    def initialize_weights(self, n_in):
        n_out = 1  
        limit = np.sqrt(2 / (n_in + n_out)) 
        self.w = np.random.randn(n_in) * limit

    # The sigmoid activation function that maps the input X to a probability.
    def sigmoid(self, X):
        Z = X.dot(self.w) + self.b 
        return 1 / (1 + np.exp(-Z))

    # Predicts output using the learned weights and sigmoid activation.
    def predict(self,X):
        return self.sigmoid(X)

    # Computes the gradient of the cost function with respect to weights and bias.
    def grad_w(self):
        y_pred = self.predict(self.X).flatten()
        return np.mean((y_pred - self.y) * y_pred * (1 - y_pred) * self.X.T, axis=1)

    def grad_b(self):
        y_pred = self.predict(self.X).flatten()
        return np.mean((y_pred - self.y) * y_pred * (1 - y_pred))

    # Updates the weights and bias terms based on the calculated gradient.
    def update(self):
        grad_w = self.grad_w()
        grad_b = self.grad_b()
        self.velocity_w = self.momentum * self.velocity_w - self.learning_rate * grad_w  
        self.velocity_b = self.momentum * self.velocity_b - self.learning_rate * grad_b  
        self.w += self.velocity_w 
        self.b += self.velocity_b

    # Trains the perceptron over a number of iterations (epochs)
    def train(self, n):
        for _ in range(n):
            self.update()